package petstore.automation.acceptance.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiResponseModel {

    public ApiResponseModel() {

    }

    private Integer code;
    private String type;
    private String message;

    public String getMessage() {
        return this.message;
    }

    public Integer getCode() {
        return this.code;
    }
}


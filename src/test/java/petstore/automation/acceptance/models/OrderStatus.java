package petstore.automation.acceptance.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderStatus {
    @JsonProperty("placed")
    Placed("placed"),
    @JsonProperty("approved")
    Approved("approved"),
    @JsonProperty("delivered")
    Delivered("delivered");

    private final String status;

    OrderStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}

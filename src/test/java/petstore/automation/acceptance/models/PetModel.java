package petstore.automation.acceptance.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class PetModel {
    private Long id;
    private PetCategoryModel category;
    private String name;
    private List<String> photoUrls;
    private List<TagModel> tags;
    private String status;

    public PetModel() 
    {
      this.photoUrls = new ArrayList<String>();
      this.tags = new ArrayList<TagModel>();
      this.category = new PetCategoryModel();
    }

    public List<String> getPhotoURLs() {
        return photoUrls;
    }

    public void setPhotoURL(String photoUrl) {
        this.photoUrls.add(photoUrl);
    }

    public void setTag(TagModel tag) {
      this.tags.add(tag);
    }

    public void setCategory(PetCategoryModel category) {
      this.category = category;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}


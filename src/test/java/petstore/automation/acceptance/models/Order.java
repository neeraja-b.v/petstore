package petstore.automation.acceptance.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Order {

    private Long id;
    private Long petId;
    // Quantity is integer as per API document but made it long to test Negative testcases
    private Long quantity;
    private String shipDate;
    private OrderStatus status;
    private Boolean complete;

    @JsonCreator
    public Order(@JsonProperty("id") Long id,
                 @JsonProperty("petId") Long petId,
                 @JsonProperty("quantity") Long quantity,
                 @JsonProperty("shipDate") String shipDate,
                 @JsonProperty("status") OrderStatus status,
                 @JsonProperty("complete") Boolean complete) {
        this.id = id;
        this.petId = petId;
        this.quantity = quantity;
        this.shipDate = shipDate;
        this.status = status;
        this.complete = complete;
    }

    public Long getPetId() {
        return this.petId;
    }

    public Long getId() {
        return this.id;
    }

    public Long getQuantity() {
        return this.quantity;
    }

    public String getShipDate() {
        return this.shipDate;
    }

    public OrderStatus getStatus() {
        return this.status;
    }

    public Boolean getComplete() {
        return this.complete;
    }

}

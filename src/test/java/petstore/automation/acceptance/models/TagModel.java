package petstore.automation.acceptance.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TagModel {

    public TagModel(){
    }

    private Long id;
    private String name;

	public void setId(Long id) {
        this.id = id;
    }
    
    public Long getId() {
        return this.id;
	}
	public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
	}
}

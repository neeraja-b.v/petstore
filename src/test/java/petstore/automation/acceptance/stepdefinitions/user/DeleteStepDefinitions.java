package petstore.automation.acceptance.stepdefinitions.user;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.models.ApiResponseModel;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class DeleteStepDefinitions extends BaseTest {
    public DeleteStepDefinitions(){
    }

    private String endPoint = "/user/{username}";

    @Given("the valid user {word}")
    public void the_valid_user_snead123(String userName) {
        Map<String,Object> pathParams = new HashMap<String, Object>();
        pathParams.put("username", userName);
        commonSteps.SetUriWithPathParmeters(pathParams);
    }

    @When("The user details delete requested")
    public void the_user_details_delete_requested() {
        commonSteps.Delete(endPoint);
    }

    @Then("The user {word} details must be deleted and returns the status {int}")
    public void the_user_details_must_be_deleted_and_returns_the_responsecode(String userName, Integer status) {
        commonSteps.CheckStatusCode(status);
        ApiResponseModel response = commonSteps.GetApiResponseModel();
        assertThat(response.getMessage().compareToIgnoreCase(userName));
    }

    @Then("The response is returned with status {int}")
    public void the_response_is_returned_with(Integer status) {
        commonSteps.CheckStatusCode(status);
    }
}

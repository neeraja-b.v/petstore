package petstore.automation.acceptance.stepdefinitions.user;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import java.util.HashMap;
import java.util.Map;

public class LoginStepDefinitions extends BaseTest {
    public LoginStepDefinitions(){
    }
    private String loginEndPoint = "/user/login";
    private String logoutEndPoint = "/user/logout";

    @When("I login with the {word} and {word}")
    public void i_login_with_the_username_and_password(String userName, String password) {
        Map<String,Object> queryParams = new HashMap<String, Object>();
        queryParams.put("username", userName);
        queryParams.put("password", password);
        commonSteps.SetUriWithQueryParmeters(queryParams);
        commonSteps.Get(loginEndPoint);
    }

    @Then("The user is logged into the system and returns the {int}")
    public void the_user_is_logged_into_the_system_and_returns_the(int status) {
        commonSteps.CheckStatusCode(status);
    }

    @When("I click on logout")
    public void i_click_on_logout() {
        commonSteps.InitRequest();
        commonSteps.Get(logoutEndPoint);
    }

    @Then("The user is logged out of the system and returns the {int}")
    public void the_user_is_logged_out_of_the_system_and_returns_the(int status) {
        commonSteps.CheckStatusCode(status);
    }
}

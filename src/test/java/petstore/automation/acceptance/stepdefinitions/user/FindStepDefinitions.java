package petstore.automation.acceptance.stepdefinitions.user;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.models.UserModel;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class FindStepDefinitions extends BaseTest {
    public FindStepDefinitions(){
    }

    private String endPoint = "/user/{username}";

    @When("The the request is made with {word} to find user")
    public void the_the_request_is_made_with_username_to_find_user(String userName) {
        Map<String,Object> queryParams = new HashMap<String, Object>();
        queryParams.put("username", userName);
        commonSteps.SetUriWithPathParmeters(queryParams);
        commonSteps.Get(endPoint);
    }

    @Then("The valid user details response with {word} and {word} is returned")
    public void user_api_returns_the_valid_pet_details(String userName, String firstName) {
        UserModel returnedUser = userSteps.GetUserModelFromResponse();
        assertThat(returnedUser.getUserName().compareToIgnoreCase(userName));
        assertThat(returnedUser.getFirstName().compareToIgnoreCase(firstName));
    }

}

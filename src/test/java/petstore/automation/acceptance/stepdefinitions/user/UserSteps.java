package petstore.automation.acceptance.stepdefinitions.user;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import petstore.automation.acceptance.models.ApiResponseModel;
import petstore.automation.acceptance.models.UserModel;

import java.util.Map;

import static helpers.PropertyReader.getProperty;

public class UserSteps {
    
    public static String BASE_URL = getProperty("BASE_URL");

    @Step
    public void CreateUser(String endpoint, UserModel userModel){
        SerenityRest.given()
                    .baseUri(BASE_URL)
                    .accept("application/json")
                    .contentType("application/json")
                    .body(userModel)
                    .post(endpoint);
    }

    @Step
    public UserModel GetUserModelFromResponse() {
        SerenityRest.then().statusCode(HttpStatus.SC_OK);
        return SerenityRest.then().extract().body().as(UserModel.class);
    }

    public UserModel GetUserModel(Long id, String userName, String firstName, String password){
        UserModel user = new UserModel();
        user.setId(id);
        user.setUserName(userName);
        user.setFirstName(firstName);
        user.setPassword(password);
        return user;
    }

    @Step
    public void UpdateUser(String endpoint, UserModel userModel){
        SerenityRest.given()
                .baseUri(BASE_URL)
                .pathParams("username", userModel.getUserName())
                .accept("application/json")
                .contentType("application/json")
                .body(userModel)
                .put(endpoint);
    }

    public UserModel GetUser(String endPoint, String userName){
        SerenityRest.given()
                .baseUri(BASE_URL)
                .pathParams("username", userName)
                .when()
                .get(endPoint);

        return this.GetUserModelFromResponse();
    }
}

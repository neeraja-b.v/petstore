package petstore.automation.acceptance.stepdefinitions.user;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.models.ApiResponseModel;
import petstore.automation.acceptance.models.UserModel;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class UserDetails extends BaseTest {
    
    private String endpoint = "/user";
    private String userEndPoint = "/user/{username}";

    @Given("The request with {word} to the find User")
    public void the_correct_request_with_username_to_the_find_user(String userName) {
        Map<String,Object> pathParams = new HashMap<String, Object>();
        pathParams.put("username", userName);
        commonSteps.SetUriWithPathParmeters(pathParams);
        commonSteps.Get(userEndPoint);
    }

    @When("I add the User to the store with details {long}, {word} , {word}, {word}")
    public void i_add_the_user_to_the_store_with_details(Long id, String userName, String firstName, String password) {
        UserModel user = userSteps.GetUserModel(id, userName, firstName, password);
        userSteps.CreateUser(endpoint, user);
    }

    @And("The User {word} must be created")
    public void the_user_must_be_created(String userName) {
        UserModel updatedUser = userSteps.GetUser(userEndPoint, userName);
        // assert
        assertThat(updatedUser.getUserName().compareToIgnoreCase(userName));
    }

    @When("The user record {word} is updated with {word}, {word}")
    public void when_user_record_update_requested(String userName, String firstName, String lastName) {
        // Get user to update
        UserModel user = userSteps.GetUser(userEndPoint, userName);

        // update fields
        user.setFirstName(firstName);
        user.setLastName(lastName);

        // update using api end point
        userSteps.UpdateUser(userEndPoint, user);

        ApiResponseModel response = commonSteps.GetApiResponseModel();
    }

    @And("User {word} is updated with {word}, {word}")
    public void user_is_updated(String userName, String firstName, String lastName) {
        // Get user to update
        UserModel updatedUser = userSteps.GetUser(userEndPoint, userName);
        // assert
        assertThat(updatedUser.getUserName().compareToIgnoreCase(userName));
        assertThat(updatedUser.getFirstName().compareToIgnoreCase(firstName));
        assertThat(updatedUser.getLastName().compareToIgnoreCase(lastName));
    }
}

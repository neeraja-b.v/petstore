package petstore.automation.acceptance.stepdefinitions.pet;

import petstore.automation.acceptance.models.PetModel;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class FindByStatusStepDefinitions extends BaseTest {

    public FindByStatusStepDefinitions(){
    }

    private String endpoint = "/pet/findByStatus";

    @Given("The correct request with {word} to the Finds Pets by status endpoint")
    public void the_correct_request_with_status_to_the_finds_pets_by_status_endpoint(String status) {
        Map<String,Object> queryParams = new HashMap<String, Object>();
        queryParams.put("status", status);
        commonSteps.SetUriWithQueryParmeters(queryParams);
    }

    @When("The request is made with the get method to Find Pets by status endpoint")
    public void the_request_is_made_with_the_get_method_to_find_pets_by_status_endpoint() {
        commonSteps.Get(endpoint);
    }

    @Then("The response with correct list of pets with {word} is returned")
    public void the_Response_With_Correct_List_Of_Pets_Is_Returned(String status) {
        List<PetModel> returnedPets = petSteps.GetPetModelsFromResponse();
        assertThat(returnedPets).isNotEmpty();
        returnedPets.forEach( x ->
                assertThat(x.getStatus()
                .compareToIgnoreCase(status)));
    }
}

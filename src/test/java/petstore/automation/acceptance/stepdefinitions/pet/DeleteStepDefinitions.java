package petstore.automation.acceptance.stepdefinitions.pet;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.models.ApiResponseModel;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;

public class DeleteStepDefinitions extends BaseTest {
    public DeleteStepDefinitions(){
    }

    private String endPoint = "/pet/{id}";

    @When("I specify the following {long} for delete")
    public void i_specify_the_following_petid(Long petId) {
        Map<String,Object> pathParams = new HashMap<String, Object>();
        pathParams.put("id", petId);
        commonSteps.SetUriWithPathParmeters(pathParams);
        commonSteps.Delete(endPoint);
    }

    @Then("The Pet details for {long} must be deleted")
    public void the_pet_details_must_be_deleted(Long petId) {
        ApiResponseModel response = commonSteps.GetApiResponseModel();
        assertThat(response.getMessage().compareToIgnoreCase(petId.toString()));
    }

    @Then("Not Found Error response returned")
    public void not_found_error_response_returned() {
        commonSteps.CheckStatusCode(HttpStatus.SC_NOT_FOUND);
    }
}

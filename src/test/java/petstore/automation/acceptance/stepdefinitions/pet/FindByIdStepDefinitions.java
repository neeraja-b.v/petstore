package petstore.automation.acceptance.stepdefinitions.pet;

import petstore.automation.acceptance.models.PetModel;
import petstore.automation.acceptance.models.ApiResponseModel;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;
import petstore.automation.acceptance.stepdefinitions.utils.CommonSteps;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

public class FindByIdStepDefinitions extends BaseTest {

    public FindByIdStepDefinitions(){
    }

    private String endPoint = "/pet/{id}";

    @When("The request is made with get method to Find the Pets by {long}")
    public void the_Request_Is_Made_With_Get_Method_To_Find_Pets_ById_Endpoint(Long petId) {
        Map<String,Object> pathParams = new HashMap<String, Object>();
        pathParams.put("id", petId);
        commonSteps.SetUriWithPathParmeters(pathParams);
        commonSteps.Get(endPoint);
    }

    @Then("Pet API returns response code {int}")
    public void pet_API_Returns_response_Code(int responseCode) {
        commonSteps.CheckStatusCode(responseCode);
    }

    @And("Pet API returns the valid pet with {long} details of {word} and {word}")
    public void pet_API_Returns_the_valid_pet_details(Long petId, String petName, String status) {
        PetModel returnedPet = petSteps.GetPetModelFromResponse();

        assertThat(returnedPet.getId() == petId);
        assertThat(returnedPet.getStatus().compareToIgnoreCase(status));
        assertThat(returnedPet.getName().compareToIgnoreCase(petName));
    }

    @Then("Pet API returns Error Message Pet not found")
    public void pet_API_returns_Error_Message() {
        String errorMessage ="Pet not found";
        ApiResponseModel response = commonSteps.GetApiResponseModel();
        assertThat(response.getMessage().compareToIgnoreCase(errorMessage));
    }
}
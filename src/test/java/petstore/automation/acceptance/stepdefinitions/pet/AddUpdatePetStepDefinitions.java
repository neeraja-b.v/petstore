package petstore.automation.acceptance.stepdefinitions.pet;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import petstore.automation.acceptance.models.PetModel;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;


public class AddUpdatePetStepDefinitions extends BaseTest {
    public AddUpdatePetStepDefinitions() {
    }

    private String endpoint = "/pet";
    private String petEndPoint = "/pet/{id}";

    @Given("I add the pet to the store with details {long}, {word} , {word} and {word}")
    public void i_add_the_pet_to_the_store_with_details(Long petId, String name, String status, String photoUrl) {
        PetModel petModel = petSteps.GetPetModel(petId, name, status, photoUrl, "category", "tag");
        petSteps.AddPetToStore(endpoint, petModel);
    }

    @When("I update the pet with {int} to the store with details {word} and {word}")
    public void i_update_the_pet_with_form_details(int petId, String name, String status) {
        petSteps.UpdatePetToStoreForm(petEndPoint, petId, name, status);;
    }

    @When("I update the Pet {int} with {word}, {word} and {word}")
    public void i_update_the_pet_with_json_details(int petId, String status, String categoryName, String tagName) {
        PetModel petModel = petSteps.GetPetModel((long)petId, "test", status, "testUrl", categoryName, tagName);

        petSteps.UpdatePetToStorePut(endpoint, petModel);
    }

    @Then("The pet details should be present with {long}, {word} , {word}")
    public void the_pet_api_returns_the_valid_pet_details(Long petId, String petName, String status) {
        PetModel returnedPet =
            SerenityRest.then().extract().body().as(PetModel.class);
    
            assertThat(returnedPet.getId() == petId);
            assertThat(returnedPet.getName().compareToIgnoreCase(petName));
            assertThat(returnedPet.getStatus().compareToIgnoreCase(status));
    }
}

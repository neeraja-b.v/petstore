package petstore.automation.acceptance.stepdefinitions.pet;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.HttpStatus;

import petstore.automation.acceptance.models.PetModel;
import petstore.automation.acceptance.models.TagModel;
import petstore.automation.acceptance.models.ApiResponseModel;
import petstore.automation.acceptance.models.PetCategoryModel;

import static helpers.PropertyReader.getProperty;

public class PetSteps {
    public static String BASE_URL = getProperty("BASE_URL");

    @Step
    public void AddPetToStore(String endpoint, PetModel petModel){
        SerenityRest.given()
                    .baseUri(BASE_URL)
                    .accept("application/json")
                    .contentType("application/json")
                    .body(petModel)
                    .post(endpoint);
    }

    @Step
    public void UpdatePetToStorePut(String endpoint, PetModel petModel){
        SerenityRest.given()
                    .baseUri(BASE_URL)
                    .accept("application/json")
                    .contentType("application/json")
                    .body(petModel)
                    .put(endpoint);
    }

    @Step
    public void UpdatePetToStoreForm(String endpoint, int petId, String name, String status){

        SerenityRest.given()
                    .baseUri(BASE_URL)
                    .pathParam("id", petId)
                    .accept("application/json")
                    .contentType("application/x-www-form-urlencoded")
                    .formParam("name", name)
                    .formParam("status", status)
                    .post(endpoint);
    }

    @Step 
    public PetModel GetPetModelFromResponse(){
        SerenityRest.then().statusCode(HttpStatus.SC_OK);
        return SerenityRest.then().extract().body().as(PetModel.class);
    }

    @Step
    public List<PetModel> GetPetModelsFromResponse(){
        SerenityRest.then().statusCode(HttpStatus.SC_OK);
        return Arrays.asList(SerenityRest.then().extract().body().as(PetModel[].class));
    }

    public PetModel GetPetModel(Long petId, String name, String status, String photoUrl,String categoryName, String tagName){
        PetModel pet = new PetModel();
        Long id = Math.abs(new Random().nextLong());
        PetCategoryModel category = new PetCategoryModel();
        category.setId(id);
        category.setName(categoryName);

        TagModel tag = new TagModel();
        tag.setId(id);
        tag.setName(tagName);

        pet.setId(petId);
        pet.setName(name);
        pet.setCategory(category);
        pet.setStatus(status);
        pet.setTag(tag);

        return pet;
    }
}

package petstore.automation.acceptance.stepdefinitions.utils;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import petstore.automation.acceptance.models.ApiResponseModel;

import java.util.Map;

import static helpers.PropertyReader.getProperty;

import static helpers.PropertyReader.getProperty;

public class CommonSteps {
    public static String BASE_URL = getProperty("BASE_URL");

    @Step
    public void CheckStatusCode(int intStatusCode){
        SerenityRest.then().statusCode(intStatusCode);
    }

    @Step
    public void checkResponseAsMap() {
        SerenityRest.then().extract().body().as(Map.class);
    }

    @Step
    public void SetBaseUri(){ SerenityRest.given().baseUri(BASE_URL);    }

    @Step
    public ApiResponseModel GetApiResponseModel(){
        return SerenityRest.then().extract().body().as(ApiResponseModel.class);
    }

    @Step
    public void SetUriWithQueryParmeters(Map<String,Object> queryParams){
        SerenityRest.given().baseUri(BASE_URL).queryParams(queryParams);
    }

    @Step
    public void SetUriWithPathParmeters(Map<String,Object> pathParams){
        SerenityRest.given().baseUri(BASE_URL).pathParams(pathParams);
    }

    @Step
    public void Get(String endPoint){
        SerenityRest.when().get(endPoint);
    }

    @Step
    public void Delete(String endPoint){
        SerenityRest.when().delete(endPoint);
    }

    @Step
    public void InitRequest(){ SerenityRest.given().baseUri(BASE_URL); }
}

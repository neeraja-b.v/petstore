package petstore.automation.acceptance.stepdefinitions.utils;

import static helpers.PropertyReader.getProperty;

import net.thucydides.core.annotations.Steps;
import petstore.automation.acceptance.stepdefinitions.pet.PetSteps;
import petstore.automation.acceptance.stepdefinitions.user.UserSteps;

public class BaseTest {
    public static String BASE_URL = getProperty("BASE_URL");

    // Steps Class of Serenity To get methods
    @Steps
    protected PetSteps petSteps;

    @Steps
    protected UserSteps userSteps;

    @Steps
    protected CommonSteps commonSteps;
}

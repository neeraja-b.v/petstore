package petstore.automation.acceptance.stepdefinitions.store;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.hc.core5.http.HttpStatus;
import petstore.automation.acceptance.models.Order;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;
import petstore.automation.acceptance.models.OrderStatus;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class OrderStepDefinitions extends BaseTest {
    private Order order;
    private String placeAnOrderEndPoint = "/store/order";
    private String petInventoryEndPoint = "/store/inventory";
    private String deleteOrderEndpoint = "/store/order/{orderId}";

    private OrderStatus getOrderStatus(String status) {
        switch (status) {
            case "placed":
                return OrderStatus.Placed;
            case "approved":
                return OrderStatus.Approved;
            case "delivered":
                return OrderStatus.Delivered;
        }
        return null;
    }

    @Given("an purchase order to buy a pet to the store with details {long}, {long} , {word} , {word}")
    public void an_purchase_order_to_buy_a_pet_to_the_store_with_details_placed_true(Long petId, Long quantity, String status, String completed) {
        order = new Order(100L, petId, quantity, LocalDateTime.now().toString(), getOrderStatus(status), Boolean.valueOf(completed));
    }

    @When("the customer attempts to place the order")
    public void the_customer_attempts_to_place_the_order() {
        customer_attempts_to_place_the_order(placeAnOrderEndPoint);
    }

    @Then("the purchase order should be success with {int}")
    public void the_purchase_order_should_be_success_with_placed(Integer responseCode) {
        commonSteps.CheckStatusCode(responseCode);
    }

    @Then("the purchase order should fail  with internal server error")
    public void the_purchase_order_should_fail_with_internal_server_error() {
        commonSteps.CheckStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @When("The call to the PetAPI is made to retrieve the pet inventory")
    public void the_call_to_the_pet_api_is_made_to_retrieve_the_pet_inventory() {
        SerenityRest.given()
                .baseUri(BASE_URL).when().get(petInventoryEndPoint);
    }

    @Then("Pet inventories by status is returned")
    public void pet_inventories_by_status_is_returned() {
        commonSteps.checkResponseAsMap();
    }

    @Given("The {long} to delete the order from database")
    public void an_to_delete_the_order_from_database(Long orderId) {
        Map<String, Object> pathParams = new HashMap<String, Object>();
        pathParams.put("orderId", orderId);
        SerenityRest.given()
                .baseUri(BASE_URL).when().pathParams(pathParams);
    }

    @When("The delete order endpoint is called with data")
    public void an_delete_order_endpoint_is_called() {
        SerenityRest.when().delete(deleteOrderEndpoint);
    }

    @Step
    public void customer_attempts_to_place_the_order(String endPoint) {
        SerenityRest.given()
                .baseUri(BASE_URL)
                .accept("application/json")
                .contentType("application/json")
                .body(order)
                .post(endPoint);
    }
}

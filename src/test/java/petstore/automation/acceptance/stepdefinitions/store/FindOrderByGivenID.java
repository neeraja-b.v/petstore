package petstore.automation.acceptance.stepdefinitions.store;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import petstore.automation.acceptance.stepdefinitions.utils.BaseTest;


public class FindOrderByGivenID extends BaseTest {

    public FindOrderByGivenID() {

    }

    private String endpoint = "/store/order/{orderId}";

    @Given("The correct request with {long} to Find Purchase order by id endpoint")
    public void the_correct_requeset_to_find_purchase_order_byid(Long orderId) {
        SerenityRest.given()
                .baseUri(BASE_URL)
                .pathParam("orderId", orderId);
    }


    @When("The request is made with get method to Find Purchase order by id endpoint")
    public void the_request_is_made_with_get_method_to_find_purchase_order_byid() {
        SerenityRest.when().get(endpoint);
    }

    @Then("Purchase order API returns response code {int}")
    public void purchase_order_API_returns_response_code(Integer responseCode) {
        SerenityRest.then()
                .statusCode(responseCode);
    }

    @Given("The non exist order with {long} to Find Purchase order by id endpoint")
    public void the_non_exist_order_with_to_find_purchase_order_byid(Long orderId) {
        SerenityRest.given()
                .baseUri(BASE_URL)
                .pathParam("orderId", orderId);
    }

    @When("The request is made with get Find Purchase order by id endpoint")
    public void the_request_is_made_with_get_find_purchase_order_byid() {
        SerenityRest.when().get(endpoint);
    }

    @Then("Pet API returns Not found response code {int}")
    public void pet_API_returns_not_found_respose_code(Integer responseCode) {
        SerenityRest.then()
                .statusCode(responseCode);
    }

}

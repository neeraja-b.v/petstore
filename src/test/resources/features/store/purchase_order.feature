Feature: PurchaseOrders

  @Positive-scenarios
  Scenario Outline:  A customer placed an order to purchase a pet
    Given an purchase order to buy a pet to the store with details <PETID>, <Quantity> , <Status> , <Complete>
    When the customer attempts to place the order
    Then the purchase order should be success with <ResponseCode>
    Examples:
      | PETID      | Quantity   | Status | Complete | ResponseCode |
      | 1          | 2          | placed | true     | 200          |
      | 2147483647 | 2147483645 | placed | true     | 200          |
      | -1         | -1         | placed | true     | 200          |
      | -1         | -1         | placed | false    | 200          |
      | 1          | 2          | placed | false    | 200          |
      | 2147483647 | 2147483645 | placed | false    | 200          |

#  API doesn't do multiple validation on the data so couldn't write more test
  @Negative-scenarios
  Scenario Outline:  A customer placed an order to purchase a pet
    Given an purchase order to buy a pet to the store with details <PETID>, <Quantity> , <Status> , <Complete>
    When the customer attempts to place the order
    Then the purchase order should fail  with internal server error
    Examples:
      | PETID | Quantity   | Status | Complete |
      | 1     | 2147483648 | placed | true     |

  @Positive-scenarios
  Scenario Outline: A api GET call with Pet API with valid end point and parameters as petId should returns pet details
    Given The correct request with <OrderId> to Find Purchase order by id endpoint
    When The request is made with get method to Find Purchase order by id endpoint
    Then Purchase order API returns response code <ResponseCode>
    Examples:
      | OrderId | ResponseCode |
      | 2       | 200          |

  @Negative-scenarios
  Scenario Outline: A api GET call with Purchase order by id API with valid end point and incorrect parameters
    Given The non exist order with <OrderId> to Find Purchase order by id endpoint
    When The request is made with get Find Purchase order by id endpoint
    Then Pet API returns Not found response code <ResponseCode>
    Examples:
      | OrderId    | ResponseCode |
      | 2147483648 | 404          |


  @Positive-scenarios
  Scenario: An api GET call to Pet API to retrieve pet inventory should returns pet inventory by status
    When The call to the PetAPI is made to retrieve the pet inventory
    Then Pet inventories by status is returned


  @Positive-scenarios
  Scenario Outline: Purchase order is successfully deleted for a given orderid present in the database
    Given The <OrderId> to delete the order from database
    When The delete order endpoint is called with data
    Then Purchase order API returns response code <ResponseCode>
    Examples:
      | OrderId | ResponseCode |
      | 100     | 200          |

  @Negative-scenarios
  Scenario Outline: Purchase order is failed to delete for a given orderid which is not present in the database
    Given The <OrderId> to delete the order from database
    When The delete order endpoint is called with data
    Then Purchase order API returns response code <ResponseCode>
    Examples:
      | OrderId     | ResponseCode |
      | 21474836484 | 404          |
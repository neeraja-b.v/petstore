Feature: Everything about your Pets
    In order to Add new Pet using POST request on the pet api
    In order to select the type of Pet I want to search using GET request
    In order to delete Pet using DELETE request on the pet api
    In order to update the Pet details using PUT request on the pet api
    In order to update the Pet details using Post request on the pet api with form data

    @Positive-Scenarios
    Scenario Outline: A api POST call with Pet API with valid end point and correct body parameters to add new pet
        Given I add the pet to the store with details <PETID>, <Name> , <Status> and <PhotoURL>
        Then The pet details should be present with <PETID>, <Name> , <Status>
        Examples:
        | PETID | Name   | Status    | PhotoURL   |
        | 67867 | Sandra | sold      | SandraCopy |
        | 59967 | Tella  | available | TellaPics  |
        | 85367 | Snead  | pending   | SneadSpecs |

    @Positive-Scenarios
    Scenario Outline: A api GET call with Pet API with valid end point and parameters as petId should returns pet details
        Given I add the pet to the store with details <PETID>, <Name> , <Status> and <PhotoURL>
        Then The pet details should be present with <PETID>, <Name> , <Status>
        When The request is made with get method to Find the Pets by <PETID>
        Then Pet API returns response code <ResponseCode>
        And Pet API returns the valid pet with <PETID> details of <Name> and <Status>
        Examples:
        | PETID | ResponseCode | Name | Status    | PhotoURL  |
        | 67897 | 200          | fish | available | fishPhoto |

    @Negative-Scenarios
    Scenario Outline: A api GET call with Pet API with valid end point and incorrect parameters
        When The request is made with get method to Find the Pets by <PETID>
        Then Pet API returns response code <ResponseCode>
        Then Pet API returns Error Message Pet not found
        Examples:
        | PETID       | ResponseCode |
        | 4567788865  | 404          |

    @Positive-Scenarios
    Scenario Outline: Find Pets By Correct Status
        Given The correct request with <status> to the Finds Pets by status endpoint
        When The request is made with the get method to Find Pets by status endpoint
        Then The response with correct list of pets with <status> is returned
        Examples:
        | status    |
        | available |
        | pending   |
        | sold      |

    @Positive-Scenarios
    Scenario Outline: Scenario: Delete a pet from the Store
        Given I add the pet to the store with details <PETID>, <Name> , <Status> and <PhotoURL>
        Then The pet details should be present with <PETID>, <Name> , <Status>
        When I specify the following <PETID> for delete
        Then The Pet details for <PETID> must be deleted
        Examples:
            | PETID    | Name   | Status    | PhotoURL   | 
            | 995067   | Sandra | sold      | SandraCopy | 
            | 8841567  | Tella  | available | TellaPics  | 
            | 7759067  | Snead  | pending   | SneadSpecs |

    @Negative-Scenarios
    Scenario Outline: Delete a pet from the Store
        When I specify the following <Id> for delete
        Then Not Found Error response returned
        Examples:
        |  Id   |
        |  80   |
        |  81   |
        |  82   |

    @Positive-Scenarios
    Scenario Outline: Update the pets details when correct ID is specified
        Given I add the pet to the store with details <PETID>, <Name> , <Status> and <PhotoURL>
        Then The pet details should be present with <PETID>, <Name> , <Status>
        When I update the Pet <PETID> with <UpdateStatus>, <TagName> and <CategoryName>
        Then The pet details should be present with <PETID>, <Name> , <UpdateStatus>
        Examples:
            | PETID | Name   | Status    | PhotoURL   | UpdateStatus | TagName         | CategoryName       |
            | 85067 | Sandra | sold      | SandraCopy | pending      | Sandra_pending  | Labrador_Retriever |
            | 41567 | Tella  | available | TellaPics  | sold         | Tella_sold      | Golden_Retriever   |
            | 59067 | Snead  | pending   | SneadSpecs | available    | Snead_available | BullDog            |

    @Positive-Scenarios
    Scenario Outline: A api Post call to Pet API with valid end point and correct form parameters to update
        Given I add the pet to the store with details <PETID>, <Name> , <Status> and <PhotoURL>
        Then The pet details should be present with <PETID>, <Name> , <Status>
        When I update the pet with <PETID> to the store with details <UpdateName> and <UpdateStatus>
        Then Pet API returns response code 200
        When The request is made with get method to Find the Pets by <PETID>
        Then The pet details should be present with <PETID>, <UpdateName> , <UpdateStatus>
        Examples:
            | PETID | Name   | Status    | PhotoURL   | UpdateStatus | UpdateName      |
            | 87767 | Sandra | sold      | SandraCopy | pending      | Sandra_pending  |
            | 48867 | Tella  | available | TellaPics  | sold         | Tella_sold      |
            | 56667 | Snead  | pending   | SneadSpecs | available    | Snead_available |
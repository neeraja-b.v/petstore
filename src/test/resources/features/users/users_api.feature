Feature: Operations about user
  In order to Add new User using POST request on the user api
  In order to select the user by user name I want to search using GET request
  In order to delete User using DELETE request on the user api
  In order to update the User details using PUT request on the user api
  In order to Login User details using get request on the user api
  In order to Logout User details using get request on the user api

  @Positive-Scenarios
  Scenario Outline: A api POST call with User API with valid end point to create new User
    When  I add the User to the store with details <id>, <userName> , <firstName>, <password>
    Then The response is returned with status <responseCode>
    And The User <userName> must be created
    Examples:
      | id    | userName | firstName | password | responseCode |
      | 1007  | Sarah123 | Sarah     | sarah123 |   200        |
      | 1008  | Tella123 | Tella     | Tella123 |   200        |
      | 1009  | Snead123 | Snead     | Snead123 |   200        |

  @Positive-Scenarios
  Scenario Outline: Update the user details when correct username is specified
    When  I add the User to the store with details <id>, <userName> , <firstName>, <password>
    Then The response is returned with status <responseCode>
    And The User <userName> must be created
    When The user record <userName> is updated with <firstNameToUpdate>, <lastNameToUpdate>
    Then The response is returned with status <responseCode>
    And User <userName> is updated with <firstNameToUpdate>, <lastNameToUpdate>
    Examples:
      | id      | userName  | firstName | password  | responseCode | firstNameToUpdate | lastNameToUpdate |
      | 223331  | Niraja123 | Niraja    | Niraja123 |   200        | nirajaupdated     | nirajalastname   |
      | 223351  | Ted123    | Ted       | Ted123    |   200        | tedupdated        | tedlastname      |

  @Negative-Scenarios
  Scenario Outline: Error is thrown when Incorrect username is specified
    Given The request with <userName> to the find User
    Then The response is returned with status <responseCode>
    Examples:
      | userName | responseCode |
      | asdh     | 404          |

  @Positive-Scenarios
  Scenario Outline: Delete an user from the system
    When  I add the User to the store with details <id>, <userName> , <firstName>, <password>
    Then The response is returned with status <responseCode>
    And The User <userName> must be created
    Given the valid user <userName>
    When The user details delete requested
    Then The user <userName> details must be deleted and returns the status <responseCode>
    Examples:
      | id       | userName | firstName | password | responseCode |
      | 8650097  | Sarah123 | Sarah     | sarah123 |   200        |
      | 5641008  | Tella123 | Tella     | Tella123 |   200        |
      | 4231009  | Snead123 | Snead     | Snead123 |   200        |

  @Positive-Scenarios
  Scenario Outline: A api GET call with Users API with valid end point and parameters as Username should returns User details
    When  I add the User to the store with details <id>, <userName> , <firstName>, <password>
    Then The response is returned with status <responseCode>
    And The User <userName> must be created
    When The the request is made with <userName> to find user
    Then The valid user details response with <userName> and <firstName> is returned
    Examples:
      | id       | userName | firstName | password | responseCode |
      | 8781197  | Sarja123 | Sarja     | sarah123 |   200        |
      | 5991228  | Tata123  | Tata      | Tella123 |   200        |

  @Negative-Scenarios
  Scenario Outline: A api GET call with Users API with valid end point and parameters as incorrect Username
    When The the request is made with <userName> to find user
    Then The response is returned with status <responseCode>
    Examples:
      | userName | responseCode |
      | xxxx     | 404          |

  @Positive-Scenarios
  Scenario Outline: A api GET call with User API with valid end point and correct body parameters to login
    When  I add the User to the store with details <id>, <userName> , <firstName>, <password>
    Then The response is returned with status <responseCode>
    And The User <userName> must be created
    When  I login with the <userName> and <password>
    Then The user is logged into the system and returns the <responseCode>
    Examples:
      | id       | userName | firstName | password | responseCode |
      | 8653197  | Sarja123 | Sarja     | sarah123 |   200        |
      | 5647828  | Tata123  | Tata      | Tella123 |   200        |

  @Positive-Scenarios
  Scenario Outline: A api GET call with User API with valid end point logs the correct user logged out of the system
    When  I click on logout
    Then The user is logged out of the system and returns the <responseCode>
    Examples:
      | responseCode |
      | 200          |